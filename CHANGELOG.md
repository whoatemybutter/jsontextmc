<h1><img src="https://gitlab.com/whoatemybutter/jsontextmc/-/raw/master/logo.png" width="64" height="64"> JSONTextMC Changelog</h1>

![Latest: v3.1.1](https://img.shields.io/static/v1?style=for-the-badge&logo=server-fault&label=latest&message=v3.1.1&color=orange)

## What's new in JSONTextMC

### 3.1.1
<small>Released: *December 28th, 2022*</small>
* **Changed to pyproject.toml**
* **Fixed some type hints**
* **Some other minor miscellaneous updates**

### 3.1
<small>Released: *May 6th, 2022*</small>
* **Reworked some areas, should perform better**
* **Fixed bugs**

### 3.0
<small>Released: *December 23rd, 2021*</small>
* **Fixed entire module**
* **Rewrote documentation**

### 2.0
<small>Released: *August 26th, 2020*</small>
* **Fixed converter**
* **Added docstrings**
* **Added package to PyPi**
* **Optimized converter for bad strings**
* **Fixed CLI front-end**

### 1.x
* **Initial release**